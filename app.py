#!/usr/bin/env python3
"""https://gitlab.com/morph027/scan-2-nextcloud"""

import os
import time
import configparser
import subprocess
import shutil
import webdav.client as wc
import sane
from flask import Flask, request, redirect, render_template

APP = Flask(__name__)

LOCKFILE = 'scan2nextcloud.lock'

def sane_scan():
    """actually scan using SANE"""
    image = None
    sane.init()
    devices = sane.get_devices()
    try:
        dev = sane.open(devices[0][0])
    except Exception as e:
        print(e)
    try:
        dev.get_parameters()
    except Exception as e:
        print(e)
    try:
        dev.depth = 8
    except Exception as e:
        print(e)
    try:
        dev.mode = 'color'
    except Exception as e:
        print(e)
    try:
        dev.resolution = 300
        dev.start()
        image = dev.snap()
        dev.close()
    except Exception as e:
        print(e)

    return image

def upload(filename):
    """upload result to webdav target"""
    client = None
    config = configparser.ConfigParser()
    config.read('/etc/scan2nextcloud.ini')
    host = config.get('webdav', 'hostname')
    folder = config.get('webdav', 'folder')
    username = config.get('webdav', 'username')
    password = config.get('webdav', 'password')
    options = {
        'webdav_hostname': host,
        'webdav_login':    username,
        'webdav_password': password,
        }
    client = wc.Client(options)
    client.upload_sync(remote_path=folder + '/' + filename, local_path=filename)
    return client

@APP.route('/', methods=['GET'])
def form():
    """starting page"""
    if os.path.isfile(LOCKFILE):
        lock = 'true'
    else:
        lock = 'false'
    state = 'info'
    return render_template('template.html', state=state, btn_id='btn', lock=lock)

@APP.route('/unlock', methods=['POST'])
def unlock():
    """remove LOCKFILE"""
    if request.form.get('unlock') == 'true':
        os.remove(LOCKFILE)
    return redirect('/')

@APP.route('/', methods=['POST'])
def scan():
    """scan page"""
    scan_format = request.form.get('scan_format')
    if os.path.isfile(LOCKFILE):
        lock = 'true'
        state = 'info'
    else:
        lock = 'false'
        open(LOCKFILE, 'a')
        image = sane_scan()
        if image:
            now = time.strftime("%Y-%m-%d_%H-%M-%S")
            if scan_format == "pdf":
                filename = now + '.pdf'
                image.save(filename)
                if shutil.which('qpdf') and shutil.which('ocrmypdf') and shutil.which('tesseract'):
                    subprocess.call(
                        ["ocrmypdf",
                         "-l",
                         "deu+eng",
                         "--skip-text",
                         filename,
                         now + '_OCR.pdf'])
                    os.remove(filename)
                    filename = now + '_OCR.pdf'
            if scan_format == "png":
                filename = now + ".png"
                image.save(filename, optimize=True)
            try:
                upload(filename)
                state = 'success'
            except Exception as e:
                print(e)
                state = 'danger'
            os.remove(filename)
        else:
            state = 'danger'
        os.remove(LOCKFILE)
    return render_template('template.html', state=state, btn_id=scan_format, lock=lock)