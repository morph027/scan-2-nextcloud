# Scan 2 Nextcloud

**Disclaimer** (sort of): This was built with purpose in mind because our new MFP is unable to scan into a SMB share. No fancy classes, methods, API, settings, stuff or lot's of logging. Just make it work as easy as possible and increase the [WAF](https://en.wikipedia.org/wiki/Wife_acceptance_factor). Put a sheet into the scanner, hit the big blue button (or the hacked Amazon Dash button) and you're done.

2 full screen buttons (target device here is any of our mobiles/tablets):

![](https://gitlab.com/morph027/scan-2-nextcloud/raw/master/success.gif) ![](https://gitlab.com/morph027/scan-2-nextcloud/raw/master/failure.gif)

This one is using SANE (configure your own first!) and WebDAV to fetch images from a scanner and store it into a Nextcloud folder. No user settings neccessary. Just push the button (We're also mis-using an Amazon Dash button :smiley:). After clicking (and scanning, uploading), it changes it's color to red on error or green on success. If you additionally install `ocrmypdf` you'll get an OCR'd text layer in your PDF which is [fulltext searchable](https://github.com/nextcloud/fulltextsearch) then.

As always, regard this as proof-of-concept or whatsoever. Feel free to modify, update, fork, complain (report issues), improve (merge requests) :smiley: I've implemented it to run synchronous (blocking) on purpose to get visual feedback for the user.

**Hint**: As this is using webdav, you can also upload to other destinations too.

## Installation

You can run this on a Pi with an USB attached scanner or any other host using a scanner over the network.

### Dependencies

#### Python 3

```bash
sudo apt-get install python3-virtualenv build-essential python3-dev libsane-dev libcurl4-openssl-dev libssl-dev
```

#### virtualenv

```bash
python3 -m virtualenv -p /usr/bin/python3 ~/virtualenv
. ~/virtualenv/bin/activate
pip3 install -r requirements.txt
```

#### Optional OCR

```bash
sudo apt-get install unpaper ghostscript tesseract-ocr tesseract-ocr-deu qpdf
```

### Install

```bash
cd /opt
git clone https://gitlab.com/morph027/scan-2-nextcloud
chown -R www-data: /opt/scan-2-nextcloud
```

### Configure

Create file ```/etc/scan2nextcloud.ini```:

```
[webdav]
hostname = https://nextcloud.example.com
folder = /remote.php/dav/files/scanner/Scanner/
username = scanner
password = password
```

### Run

#### gunicorn

```
gunicorn -b 0.0.0.0:5000 --workers 2 --timeout 60 app:APP
```
